// --------------------------------------------------------------------------------
// Copyright 2002-2022 Echo Three, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// --------------------------------------------------------------------------------

package com.echothree.ui.cli.eventsToItemEvents.schema;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.IOException;

public abstract class BaseTypeAdapter<T>
        extends TypeAdapter<T> {

    Long discardNullLong(JsonReader jsonReader)
            throws IOException {
        jsonReader.nextNull();
        return null;
    }

    Integer discardNullInteger(JsonReader jsonReader)
            throws IOException {
        jsonReader.nextNull();
        return null;
    }

    String discardNullString(JsonReader jsonReader)
            throws IOException {
        jsonReader.nextNull();
        return null;
    }

    Long nextLongOrNull(JsonReader jsonReader)
            throws IOException {
        return jsonReader.peek() == JsonToken.NULL ? discardNullLong(jsonReader) : jsonReader.nextLong();
    }

    Integer nextIntegerOrNull(JsonReader jsonReader)
            throws IOException {
        return jsonReader.peek() == JsonToken.NULL ? discardNullInteger(jsonReader) : jsonReader.nextInt();
    }

    String nextStringOrNull(JsonReader jsonReader)
            throws IOException {
        return jsonReader.peek() == JsonToken.NULL ? discardNullString(jsonReader) : jsonReader.nextString();
    }

}
