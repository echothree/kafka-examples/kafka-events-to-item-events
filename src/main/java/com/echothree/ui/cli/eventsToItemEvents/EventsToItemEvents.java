// --------------------------------------------------------------------------------
// Copyright 2002-2022 Echo Three, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// --------------------------------------------------------------------------------

package com.echothree.ui.cli.eventsToItemEvents;

import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventsToItemEvents
        extends BaseGraphQlEventRelay {

    private static final Logger LOG = LoggerFactory.getLogger(EventsToItemEvents.class);

    public EventsToItemEvents(final String bootstrapServers, final String incomingTopic, final String outgoingTopic,
            final String username, final String password) {
        super(bootstrapServers, incomingTopic, outgoingTopic, username, password);
    }

    protected String executeItemQuery(final CloseableHttpClient client, final String id)
            throws Exception {
        var responseJson = executeUsingPost("""
            query {
                item(
                    id: "%s"
                ) {
                    itemName
                    description
                    id
                    itemPriceType {
                        itemPriceTypeName
                        description
                        id
                    }
                    itemPrices {
                        totalCount
                        edges {
                            node {
                                inventoryCondition {
                                    inventoryConditionName
                                    description
                                    id
                                }
                                unitOfMeasureType {
                                    unitOfMeasureTypeName
                                    description
                                    id
                                }
                                currency {
                                    currencyIsoName
                                    description
                                    id
                                }
                                maximumUnitPrice
                                minimumUnitPrice
                                unitPriceIncrement
                                unitPrice
                            }
                        }
                    }
                    entityInstance {
                        entityAppearance {
                            appearance {
                                appearanceName
                                appearanceTextDecorations {
                                    textDecoration {
                                        textDecorationName
                                    }
                                }
                                fontStyle {
                                    fontStyleName
                                }
                                fontWeight {
                                    fontWeightName
                                }
                                textColor {
                                    red
                                    green
                                    blue
                                }
                                backgroundColor {
                                    red
                                    green
                                    blue
                                }
                            }
                        }
                        entityAttributeGroups {
                            entityAttributeGroupName
                            description
                            entityAttributes {
                                entityAttributeName
                                description
                                entityAttributeType {
                                    entityAttributeTypeName
                                }
                                attribute {
                                    __typename
                                    ... on EntityBooleanAttribute {
                                        booleanAttribute
                                    }
                                    ... on EntityIntegerAttribute {
                                        integerAttribute
                                    }
                                    ... on EntityLongAttribute {
                                        longAttribute
                                    }
                                    ... on EntityListItemAttribute {
                                        entityListItem {
                                            entityListItemName
                                            description
                                        }
                                    }
                                    ... on EntityNameAttribute {
                                        nameAttribute
                                    }
                                    ... on EntityStringAttribute {
                                        language {
                                            languageIsoName
                                        }
                                        stringAttribute
                                    }
                                    ... on EntityClobAttribute {
                                        language {
                                            languageIsoName
                                        }
                                        clobAttribute
                                        mimeType {
                                            mimeTypeName
                                        }
                                    }
                                    ... on EntityGeoPointAttribute {
                                        latitude
                                        unformattedLatitude
                                        longitude
                                        unformattedLongitude
                                        elevation
                                        unformattedElevation
                                        altitude
                                        unformattedAltitude
                                    }
                                    ... on EntityDateAttribute {
                                        dateAttribute
                                        unformattedDateAttribute
                                    }
                                    ... on EntityTimeAttribute {
                                        timeAttribute
                                        unformattedTimeAttribute
                                    }
                                    ... on EntityEntityAttribute {
                                        entityInstanceAttribute {
                                            ulid
                                            entityNames {
                                                target
                                                names {
                                                    name
                                                    value
                                                }
                                            }
                                            description
                                        }
                                    }
                                    ... on EntityMultipleListItemAttributes {
                                        entityListItems {
                                            entityListItemName
                                            description
                                        }
                                    }
                                    ... on EntityCollectionAttributes {
                                        entityInstanceAttributes {
                                            ulid
                                            entityNames {
                                                target
                                                names {
                                                    name
                                                    value
                                                }
                                            }
                                            description
                                        }
                                    }
                                }
                                unitOfMeasureType {
                                    singularDescription
                                    pluralDescription
                                    unitOfMeasureKind {
                                        description
                                    }
                                }
                                entityListItemSequence {
                                    description
                                }
                                entityAttributeEntityAttributeGroups {
                                    entityAttributeGroup {
                                        entityAttributeGroupName
                                    }
                                }
                            }
                        }
                    }
                }
            }
            """.formatted(id), client);

        // Remove the 'data' wrapping the item.
        return toJson(getMap(toMap(responseJson), "data"));
    }

    @Override
    protected String execute(final String id, final CloseableHttpClient client)
            throws Exception {
        var hasErrors = executeEmployeeLoginMutation(client);
        String json = null;

        if(!hasErrors){
            json = executeItemQuery(client, id);
        }

        return json;
    }

    public void run()
            throws Exception {
        var groupId = EventsToItemEvents.class.getSimpleName();

        eventLoop(groupId, "ECHOTHREE", "Item");
    }

}
